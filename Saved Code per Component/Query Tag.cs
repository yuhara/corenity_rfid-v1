        private void btn_QueryTag_Click(object sender, EventArgs e)
        {
            if (chb_TID.Checked)
            {
                if ((txt_StartAddress.Text.Length) != 2 || ((txt_LEN.Text.Length) != 2))
                {
                    toolStripStatusLabel1.Text = "TID Parameter Error！";
                    //StatusBar1.Panels[0].Text = "TID Parameter Error！";
                    return;
                }
            }

            timer_test.Enabled = !timer_test.Enabled;

            if (!timer_test.Enabled)
            {
                txt_StartAddress.Enabled = true;
                txt_LEN.Enabled = true;
                chb_TID.Enabled = true;

                if (ListView1_EPC.Items.Count != 0)
                {
                    chb_TID.Enabled = true;

                    //DestroyCode.Enabled = false;
                    //AccessCode.Enabled = false;
                    //NoProect.Enabled = false;
                    //Proect.Enabled = false;
                    //Always.Enabled = false;
                    //AlwaysNot.Enabled = false;
                    //NoProect2.Enabled = true;
                    //Proect2.Enabled = true;
                    //Always2.Enabled = true;
                    //AlwaysNot2.Enabled = true;
                    //P_Reserve.Enabled = true;
                    //P_EPC.Enabled = true;
                    //P_TID.Enabled = true;
                    //P_User.Enabled = true;
                    //Button_DestroyCard.Enabled = true;
                    //Button_SetReadProtect_G2.Enabled = true;
                    //Button_SetEASAlarm_G2.Enabled = true;
                    //Alarm_G2.Enabled = true;
                    //NoAlarm_G2.Enabled = true;
                    //Button_LockUserBlock_G2.Enabled = true;
                    //Button_WriteEPC_G2.Enabled = true;
                    //Button_SetMultiReadProtect_G2.Enabled = true;
                    //Button_RemoveReadProtect_G2.Enabled = true;
                    //Button_CheckReadProtected_G2.Enabled = true;
                    //button4.Enabled = true;
                    //SpeedButton_Read_G2.Enabled = true;
                    //Button_SetProtectState.Enabled = true;
                    //Button_DataWrite.Enabled = true;
                    //BlockWrite.Enabled = true;
                    //Button_BlockErase.Enabled = true
                }
                if (ListView1_EPC.Items.Count == 0)
                {
                    //DestroyCode.Enabled = false;
                    //AccessCode.Enabled = false;
                    //NoProect.Enabled = false;
                    //Proect.Enabled = false;
                    //Always.Enabled = false;
                    //AlwaysNot.Enabled = false;
                    //NoProect2.Enabled = false;
                    //Proect2.Enabled = false;
                    //Always2.Enabled = false;
                    //AlwaysNot2.Enabled = false;
                    //P_Reserve.Enabled = false;
                    //P_EPC.Enabled = false;
                    //P_TID.Enabled = false;
                    //P_User.Enabled = false;
                    //Button_DestroyCard.Enabled = false;
                    //Button_SetReadProtect_G2.Enabled = false;
                    //Button_SetEASAlarm_G2.Enabled = false;
                    //Alarm_G2.Enabled = false;
                    //NoAlarm_G2.Enabled = false;
                    //Button_LockUserBlock_G2.Enabled = false;
                    //SpeedButton_Read_G2.Enabled = false;
                    //Button_DataWrite.Enabled = false;
                    //BlockWrite.Enabled = false;
                    //Button_BlockErase.Enabled = false;
                    //Button_WriteEPC_G2.Enabled = true;
                    //Button_SetMultiReadProtect_G2.Enabled = true;
                    //Button_RemoveReadProtect_G2.Enabled = true;
                    //Button_CheckReadProtected_G2.Enabled = true;
                    //button4.Enabled = true;
                    //Button_SetProtectState.Enabled = false;
                    chb_TID.Enabled = false;
                }

                AddCmdLog("Inventory", "Exit Query", 0);
                btn_QueryTag.Text = "Stop";
            }
            else
            {
                txt_StartAddress.Enabled = false;
                txt_LEN.Enabled = false;
                chb_TID.Enabled = false;

                ListView1_EPC.Items.Clear();

                btn_QueryTag.Text = "Stop";
                chb_TID.Enabled = false;

                //DestroyCode.Enabled = false;
                //AccessCode.Enabled = false;
                //NoProect.Enabled = false;
                //Proect.Enabled = false;
                //Always.Enabled = false;
                //AlwaysNot.Enabled = false;
                //NoProect2.Enabled = false;
                //Proect2.Enabled = false;
                //Always2.Enabled = false;
                //AlwaysNot2.Enabled = false;
                //P_Reserve.Enabled = false;
                //P_EPC.Enabled = false;
                //P_TID.Enabled = false;
                //P_User.Enabled = false;
                //Button_WriteEPC_G2.Enabled = false;
                //Button_SetMultiReadProtect_G2.Enabled = false;
                //Button_RemoveReadProtect_G2.Enabled = false;
                //Button_CheckReadProtected_G2.Enabled = false;
                //button4.Enabled = false;

                //Button_DestroyCard.Enabled = false;
                //Button_SetReadProtect_G2.Enabled = false;
                //Button_SetEASAlarm_G2.Enabled = false;
                //Alarm_G2.Enabled = false;
                //NoAlarm_G2.Enabled = false;
                //Button_LockUserBlock_G2.Enabled = false;
                //SpeedButton_Read_G2.Enabled = false;
                //Button_DataWrite.Enabled = false;
                //BlockWrite.Enabled = false;
                //Button_BlockErase.Enabled = false;
                //Button_SetProtectState.Enabled = false;

                //ComboBox_EPC1.Items.Clear();
                //ComboBox_EPC2.Items.Clear();
                //ComboBox_EPC3.Items.Clear();
                //ComboBox_EPC4.Items.Clear();
                //ComboBox_EPC5.Items.Clear();
                //ComboBox_EPC6.Items.Clear();
            }
        }
