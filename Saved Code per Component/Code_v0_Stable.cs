using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Resources;
using System.Reflection;
using System.IO.Ports;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using ReaderB;

namespace Corenity_RFID
{
    public partial class Form1 : Form
    {
        // [STAThread]
        private bool fAppClosed; //在测试模式下响应关闭应用程序
        private byte fComAdr = 0xff; //当前操作的ComAdr
        private int ferrorcode;
        private byte fBaud;
        //private double fdminfre;
        //private double fdmaxfre;
        //private byte Maskadr;
        //private byte MaskLen;
        //private byte MaskFlag;
        private int fCmdRet = 30; //所有执行指令的返回值
        private int fOpenComIndex; //打开的串口索引号
        private bool fIsInventoryScan;
        //private bool fisinventoryscan_6B;
        private byte[] fOperEPC = new byte[36];
        private byte[] fPassWord = new byte[4];
        private byte[] fOperID_6B = new byte[8];
        //private int CardNum1 = 0;
        ArrayList list = new ArrayList();
        //private bool fTimer_6B_ReadWrite;
        private string fInventory_EPC_List; //存贮询查列表（如果读取的数据没有变化，则不进行刷新）
        private int frmcomportindex;
        private bool ComOpen = false;
        private bool breakflag = false;
        //private double x_z;
        //private double y_f;
        //The following TCPIP configuration required variables (以下TCPIP配置所需变量)
        public string fRecvUDPstring = "";
        public string RemostIP = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void refreshStatus()
        {
            if (!(cmb_OpenedCOM.Items.Count != 0))
                toolStripStatusLabel1.Text = "COM Closed";
            //StatusBar1.Panels[1].Text = "COM Closed";
            else
                toolStripStatusLabel1.Text = " COM" + Convert.ToString(frmcomportindex);
            toolStripStatusLabel2.Text = "";
            toolStripStatusLabel3.Text = "";

            //StatusBar1.Panels[1].Text = " COM" + Convert.ToString(frmcomportindex);
            //StatusBar1.Panels[0].Text = "";
            //StatusBar1.Panels[2].Text = "";
        }

        private string getReturnCodeDesc(int cmdRet)
        {
            switch (cmdRet)
            {
                case 0x00:
                    return "Operation Successed";
                case 0x01:
                    return "Return before Inventory finished";
                case 0x02:
                    return "the Inventory-scan-time overflow";
                case 0x03:
                    return "More Data";
                case 0x04:
                    return "Reader module MCU is Full";
                case 0x05:
                    return "Access Password Error";
                case 0x09:
                    return "Destroy Password Error";
                case 0x0a:
                    return "Destroy Password Error Cannot be Zero";
                case 0x0b:
                    return "Tag Not Support the command";
                case 0x0c:
                    return "Use the commmand,Access Password Cannot be Zero";
                case 0x0d:
                    return "Tag is protected,cannot set it again";
                case 0x0e:
                    return "Tag is unprotected,no need to reset it";
                case 0x10:
                    return "There is some locked bytes,write fail";
                case 0x11:
                    return "can not lock it";
                case 0x12:
                    return "is locked,cannot lock it again";
                case 0x13:
                    return "Parameter Save Fail,Can Use Before Power";
                case 0x14:
                    return "Cannot adjust";
                case 0x15:
                    return "Return before Inventory finished";
                case 0x16:
                    return "Inventory-Scan-Time overflow";
                case 0x17:
                    return "More Data";
                case 0x18:
                    return "Reader module MCU is full";
                case 0x19:
                    return "Not Support Command Or AccessPassword Cannot be Zero";
                case 0xFA:
                    return "Get Tag,Poor Communication,Inoperable";
                case 0xFB:
                    return "No Tag Operable";
                case 0xFC:
                    return "Tag Return ErrorCode";
                case 0xFD:
                    return "Command length wrong";
                case 0xFE:
                    return "Illegal command";
                case 0xFF:
                    return "Parameter Error";
                case 0x30:
                    return "Communication error";
                case 0x31:
                    return "CRC checksummat error";
                case 0x32:
                    return "Return data length error";
                case 0x33:
                    return "Communication busy";
                case 0x34:
                    return "Busy,command is being executed";
                case 0x35:
                    return "ComPort Opened";
                case 0x36:
                    return "ComPort Closed";
                case 0x37:
                    return "Invalid Handle";
                case 0x38:
                    return "Invalid Port";
                case 0xEE:
                    return "Return command error";
                default:
                    return "";
            }
        }

        private string getErrorCodeDesc(int cmdRet)
        {
            switch (cmdRet)
            {
                case 0x00:
                    return "Other error";
                case 0x03:
                    return "Memory out or pc not support";
                case 0x04:
                    return "Memory Locked and unwritable";
                case 0x0b:
                    return "No Power,memory write operation cannot be executed";
                case 0x0f:
                    return "Not Special Error,tag not support special errorcode";
                default:
                    return "";
            }
        }

        private byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        private string ByteArrayToHexString(byte[] data)
        {
            StringBuilder sb = new StringBuilder(data.Length * 3);
            foreach (byte b in data)
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
            return sb.ToString().ToUpper();
        }

        private void AddCmdLog(string CMD, string cmdStr, int cmdRet)
        {
            try
            {
                toolStripStatusLabel1.Text = "";
                toolStripStatusLabel1.Text = DateTime.Now.ToLongTimeString() + " " +
                                             cmdStr + ": " +
                                             getReturnCodeDesc(cmdRet);

                //StatusBar1.Panels[0].Text = "";
                //StatusBar1.Panels[0].Text = DateTime.Now.ToLongTimeString() + " " +
                //                            cmdStr + ": " +
                //                            GetReturnCodeDesc(cmdRet);
            }
            finally
            {
                ;
            }
        }

        private void AddCmdLog(string CMD, string cmdStr, int cmdRet, int errocode)
        {
            try
            {
                toolStripStatusLabel1.Text = "";
                toolStripStatusLabel1.Text = DateTime.Now.ToLongTimeString() + " " +
                                             cmdStr + ": " +
                                             getReturnCodeDesc(cmdRet) + " " + "0x" + Convert.ToString(errocode, 16).PadLeft(2, '0');

                //StatusBar1.Panels[0].Text = "";
                //StatusBar1.Panels[0].Text = DateTime.Now.ToLongTimeString() + " " +
                //                            cmdStr + ": " +
                //                            GetReturnCodeDesc(cmdRet) + " " + "0x" + Convert.ToString(errocode, 16).PadLeft(2, '0');
            }
            finally
            {
                ;
            }
        }

        private void clearLastInfo()
        {
            cmb_OpenedCOM.Refresh();
            refreshStatus();

            //Edit_Type.Text = "";
            //Edit_Version.Text = "";
            //ISO180006B.Checked = false;
            //EPCC1G2.Checked = false;
            //Edit_ComAdr.Text = "";
            //Edit_powerdBm.Text = "";
            //Edit_scantime.Text = "";
            //Edit_dminfre.Text = "";
            //Edit_dmaxfre.Text = "";
            ////  PageControl1.TabIndex = 0;
        }

        private void initComList()
        {
            int i = 0;
            cmb_COM.Items.Clear();
            cmb_COM.Items.Add(" AUTO");

            for (i = 1; i < 13; i++)
                cmb_COM.Items.Add(" COM" + Convert.ToString(i));

            cmb_COM.SelectedIndex = 0;
            refreshStatus();
        }

        private void InitReaderList()
        {
            int i = 0;
            //// ComboBox_PowerDbm.SelectedIndex = 0;
            //ComboBox_baud.SelectedIndex = 3;
            //for (i = 0; i < 63; i++)
            //{
            //    ComboBox_dminfre.Items.Add(Convert.ToString(902.6 + i * 0.4) + " MHz");
            //    ComboBox_dmaxfre.Items.Add(Convert.ToString(902.6 + i * 0.4) + " MHz");
            //}
            //ComboBox_dmaxfre.SelectedIndex = 62;
            //ComboBox_dminfre.SelectedIndex = 0;
            //for (i = 0x03; i <= 0xff; i++)
            //    ComboBox_scantime.Items.Add(Convert.ToString(i) + "*100ms");
            //ComboBox_scantime.SelectedIndex = 7;
            i = 40;

            while (i <= 300)
            {
                cmb_QueryInterval.Items.Add(Convert.ToString(i) + "ms");
                i = i + 10;
            }
            cmb_QueryInterval.SelectedIndex = 1;

            i = 40;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            progressBar.Visible = false;
            fOpenComIndex = -1;
            fComAdr = 0;
            ferrorcode = -1;
            fBaud = 5;
            initComList();
            InitReaderList();

            //NoAlarm_G2.Checked = true;
            //Byone_6B.Checked = true;
            //Different_6B.Checked = true;

            //P_EPC.Checked = true;
            //C_EPC.Checked = true;
            //DestroyCode.Checked = true;
            //NoProect.Checked = true;
            //NoProect2.Checked = true;

            fAppClosed = false;
            fIsInventoryScan = false;

            //fisinventoryscan_6B = false;
            //fTimer_6B_ReadWrite = false;

            //Label_Alarm.Visible = false;

            timer_test.Enabled = false;
            timer_G2Read.Enabled = false;
            timer_G2Alarm.Enabled = false;
            timer1.Enabled = false;

            btn_QueryTag.Text = "Query Tag";
            btn_QueryTag.Enabled = false;

            //Button3.Enabled = false;
            //Button5.Enabled = false;
            //Button1.Enabled = false;

            //button20.Enabled = false;
            //Button_DestroyCard.Enabled = false;
            //Button_WriteEPC_G2.Enabled = false;
            //Button_SetReadProtect_G2.Enabled = false;
            //Button_SetMultiReadProtect_G2.Enabled = false;
            //Button_RemoveReadProtect_G2.Enabled = false;
            //Button_CheckReadProtected_G2.Enabled = false;
            //Button_SetEASAlarm_G2.Enabled = false;
            //button4.Enabled = false;
            //Button_LockUserBlock_G2.Enabled = false;
            //SpeedButton_Read_G2.Enabled = false;
            //Button_DataWrite.Enabled = false; //Not Yet Implemented
            //BlockWrite.Enabled = false;
            //Button_BlockErase.Enabled = false;
            //Button_SetProtectState.Enabled = false;
            //SpeedButton_Query_6B.Enabled = false;
            //SpeedButton_Read_6B.Enabled = false;
            //SpeedButton_Write_6B.Enabled = false;
            //Button14.Enabled = false;
            //Button15.Enabled = false;

            //  Radio Button, EPCC1-G2 Tab
            //DestroyCode.Enabled = false;
            //AccessCode.Enabled = false;
            //NoProect.Enabled = false;
            //Proect.Enabled = false;
            //Always.Enabled = false;
            //AlwaysNot.Enabled = false;

            //  Radio Button, EPCC1-G2 Tab
            //NoProect2.Enabled = false;
            //Proect2.Enabled = false;
            //Always2.Enabled = false;
            //AlwaysNot2.Enabled = false;
            //P_Reserve.Enabled = false;
            //P_EPC.Enabled = false;
            //P_TID.Enabled = false;
            //P_User.Enabled = false;

            //  Radio Button, 18000-6B Tab
            //Same_6B.Enabled = false;
            //Different_6B.Enabled = false;
            //Less_6B.Enabled = false;
            //Greater_6B.Enabled = false;

            //  Radio Button, Reader Parameter Tab
            //radioButton1.Checked = true;
            //radioButton4.Checked = true;
            //radioButton5.Checked = true;
            //radioButton7.Checked = true;
            //radioButton10.Checked = true;
            //radioButton14.Checked = true;
            //button6.Enabled = false;
            //button8.Enabled = false;
            //button9.Enabled = false;
            //button10.Enabled = false;
            //button11.Enabled = false;
            //comboBox5.Enabled = false;

            //radioButton5.Enabled = false;
            //radioButton6.Enabled = false;
            //radioButton7.Enabled = false;
            //radioButton8.Enabled = false;
            //radioButton9.Enabled = false;
            //radioButton10.Enabled = false;
            //radioButton11.Enabled = false;
            //radioButton12.Enabled = false;
            //radioButton13.Enabled = false;
            //radioButton14.Enabled = false;
            //radioButton15.Enabled = false;
            //textBox3.Enabled = false;
            //radioButton_band1.Checked = true;
            //radioButton16.Enabled = false;
            //radioButton17.Enabled = false;
            //radioButton18.Enabled = false;
            //radioButton19.Enabled = false;
            //radioButton16.Checked = true

            //comboBox9.SelectedIndex = 0;
            //comboBox10.SelectedIndex = 0;

            cmb_BaudInput.SelectedIndex = 3;
            rdbtn_COM.Checked = true;
            chb_TID.Enabled = true;
        }

        private void Inventory()
        {
            int i;
            int CardNum = 0;
            int Totallen = 0;
            int EPClen, m;
            byte[] EPC = new byte[5000];
            int CardIndex;
            string temps;
            string s, sEPC;
            bool isonlistview;
            fIsInventoryScan = true;
            byte AdrTID = 0;
            byte LenTID = 0;
            byte TIDFlag = 0;
            if (chb_TID.Checked)
            {
                AdrTID = Convert.ToByte(txt_StartAddress.Text, 16);
                LenTID = Convert.ToByte(txt_LEN.Text, 16);
                TIDFlag = 1;
            }
            else
            {
                AdrTID = 0;
                LenTID = 0;
                TIDFlag = 0;
            }
            ListViewItem aListItem = new ListViewItem();
            fCmdRet = StaticClassReaderB.Inventory_G2(ref fComAdr, AdrTID, LenTID, TIDFlag, EPC, ref Totallen, ref CardNum, frmcomportindex);
            if ((fCmdRet == 1) | (fCmdRet == 2) | (fCmdRet == 3) | (fCmdRet == 4) | (fCmdRet == 0xFB))
            //The representative has finished searching, (代表已查找结束，)
            {
                byte[] daw = new byte[Totallen];
                Array.Copy(EPC, daw, Totallen);
                temps = ByteArrayToHexString(daw);
                fInventory_EPC_List = temps;            //Storage record (存贮记录)
                m = 0;

                /*   while (ListView1_EPC.Items.Count < CardNum)
                  {
                      aListItem = ListView1_EPC.Items.Add((ListView1_EPC.Items.Count + 1).ToString());
                      aListItem.SubItems.Add("");
                      aListItem.SubItems.Add("");
                      aListItem.SubItems.Add("");
                 * 
                  }
                */

                if (CardNum == 0)
                {
                    fIsInventoryScan = false;
                    return;
                }
                for (CardIndex = 0; CardIndex < CardNum; CardIndex++)
                {
                    EPClen = daw[m];
                    sEPC = temps.Substring(m * 2 + 2, EPClen * 2);
                    m = m + EPClen + 1;
                    if (sEPC.Length != EPClen * 2)
                        return;
                    isonlistview = false;
                    for (i = 0; i < ListView1_EPC.Items.Count; i++)     //判断是否在Listview列表内
                    {
                        if (sEPC == ListView1_EPC.Items[i].SubItems[1].Text)
                        {
                            aListItem = ListView1_EPC.Items[i];
                            ChangeSubItem(aListItem, 1, sEPC);
                            isonlistview = true;
                        }
                    }
                    if (!isonlistview)
                    {
                        aListItem = ListView1_EPC.Items.Add((ListView1_EPC.Items.Count + 1).ToString());
                        aListItem.SubItems.Add("");
                        aListItem.SubItems.Add("");
                        aListItem.SubItems.Add("");
                        s = sEPC;
                        ChangeSubItem(aListItem, 1, s);
                        s = (sEPC.Length / 2).ToString().PadLeft(2, '0');
                        ChangeSubItem(aListItem, 2, s);
                        if (!chb_TID.Checked)
                        {
                            //if (ComboBox_EPC1.Items.IndexOf(sEPC) == -1)
                            //{
                            //    ComboBox_EPC1.Items.Add(sEPC);
                            //    ComboBox_EPC2.Items.Add(sEPC);
                            //    ComboBox_EPC3.Items.Add(sEPC);
                            //    ComboBox_EPC4.Items.Add(sEPC);
                            //    ComboBox_EPC5.Items.Add(sEPC);
                            //    ComboBox_EPC6.Items.Add(sEPC);
                            //}
                        }

                    }
                }
            }
            if (!chb_TID.Checked)
            {
                //if ((ComboBox_EPC1.Items.Count != 0))
                //{
                //    ComboBox_EPC1.SelectedIndex = 0;
                //    ComboBox_EPC2.SelectedIndex = 0;
                //    ComboBox_EPC3.SelectedIndex = 0;
                //    ComboBox_EPC4.SelectedIndex = 0;
                //    ComboBox_EPC5.SelectedIndex = 0;
                //    ComboBox_EPC6.SelectedIndex = 0;
                //}
            }
            fIsInventoryScan = false;
            if (fAppClosed)
                Close();
        }

        private void timer_test_Tick(object sender, EventArgs e)
        {
            if (fIsInventoryScan)
                return;
            Inventory();
        }

        private void cmb_COM_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmb_BaudInput.Items.Clear();
            if (cmb_COM.SelectedIndex == 0)
            {
                cmb_BaudInput.Items.Add("9600bps");
                cmb_BaudInput.Items.Add("19200bps");
                cmb_BaudInput.Items.Add("38400bps");
                cmb_BaudInput.Items.Add("57600bps");
                cmb_BaudInput.Items.Add("115200bps");
                cmb_BaudInput.SelectedIndex = 3;
            }
            else
            {
                cmb_BaudInput.Items.Add("Auto");
                cmb_BaudInput.SelectedIndex = 0;
            }
        }

        private void btn_OpenNetPort_Click(object sender, EventArgs e)
        {
            int port, openresult = 0;
            string IPAddr;
            if (txt_TCPAddress.Text == "")
                txt_ComAddress.Text = "FF";
            fComAdr = Convert.ToByte(txt_TCPAddress.Text, 16); // $FF;
            if ((txt_TCPPort.Text == "") || (txt_TCPIPAddress.Text == ""))
                MessageBox.Show("Config error!", "information");
            port = Convert.ToInt32(txt_TCPPort.Text);
            IPAddr = txt_TCPIPAddress.Text;

            openresult = StaticClassReaderB.OpenNetPort(port, IPAddr, ref fComAdr, ref frmcomportindex);

            fOpenComIndex = frmcomportindex;
            if (openresult == 0)
            {
                ComOpen = true;
                //btn_GetReaderInfo_Click(sender, e); //Automatically read the reader information - 自动执行读取写卡器信息
            }
            if ((openresult == 0x35) || (openresult == 0x30))
            {
                MessageBox.Show("TCPIP error", "Information");
                StaticClassReaderB.CloseNetPort(frmcomportindex);
                ComOpen = false;
                return;
            }
            if ((fOpenComIndex != -1) && (openresult != 0X35) && (openresult != 0X30))
            {
                btn_QueryTag.Enabled = true;
                ComOpen = true;

                //btn_GetReaderInfo.Enabled = true;
                //button20.Enabled = true;
                //Button5.Enabled = true;
                //Button1.Enabled = true
                //Button_WriteEPC_G2.Enabled = true;
                //Button_SetMultiReadProtect_G2.Enabled = true;
                //Button_RemoveReadProtect_G2.Enabled = true;
                //Button_CheckReadProtected_G2.Enabled = true;
                //button4.Enabled = true;
                //SpeedButton_Query_6B.Enabled = true;
                //button6.Enabled = true
                //button8.Enabled = true;
                //button9.Enabled = true;
                //button12.Enabled = true;
                //button_OffsetTime.Enabled = true;
                //button_settigtime.Enabled = true;
                //button_gettigtime.Enabled = true;
            }
            if ((fOpenComIndex == -1) && (openresult == 0x30))
                MessageBox.Show("TCPIP Communication Error", "Information");
            refreshStatus();
        }

        private void btn_CloseNetPort_Click(object sender, EventArgs e)
        {
            clearLastInfo();
            fCmdRet = StaticClassReaderB.CloseNetPort(frmcomportindex);
            if (fCmdRet == 0)
            {
                fOpenComIndex = -1;
                refreshStatus();

                btn_QueryTag.Enabled = false;

                ListView1_EPC.Items.Clear();

                btn_QueryTag.Text = Convert.ToString(fCmdRet);

                ComOpen = false;

                timer1.Enabled = false;
            }
        }

        private void rdbtn_COM_CheckedChanged(object sender, EventArgs e)
        {
            btn_OpenComPort.Enabled = true;
            btn_CloseComPort.Enabled = true;
            btn_OpenNetPort.Enabled = false;
            btn_CloseNetPort.Enabled = false;
            btn_CloseNetPort_Click(sender, e);
        }

        private void rdbtn_TCP_CheckedChanged(object sender, EventArgs e)
        {
            if (cmb_OpenedCOM.Items.Count > 0)
                btn_CloseComPort_Click(sender, e);
            btn_OpenComPort.Enabled = false;
            btn_CloseComPort.Enabled = false;
            btn_OpenNetPort.Enabled = true;
            btn_CloseNetPort.Enabled = true;
        }

        private void btn_OpenComPort_Click(object sender, EventArgs e)
        {
            int port = 0;
            int openresult, i;
            openresult = 30;
            string temp;
            Cursor = Cursors.WaitCursor;
            if (txt_ComAddress.Text == "")
                txt_ComAddress.Text = "FF";
            fComAdr = Convert.ToByte(txt_ComAddress.Text, 16); // $FF;
            try
            {
                if (cmb_COM.SelectedIndex == 0) //Auto
                {
                    fBaud = Convert.ToByte(cmb_BaudInput.SelectedIndex);
                    if (fBaud > 2)
                    {
                        fBaud = Convert.ToByte(fBaud + 2);
                    }
                    openresult = StaticClassReaderB.AutoOpenComPort(ref port, ref fComAdr, fBaud, ref frmcomportindex);
                    fOpenComIndex = frmcomportindex;
                    if (openresult == 0)
                    {
                        ComOpen = true;
                        // Button3_Click(sender, e); //Automatically read the reader information (自动执行读取写卡器信息)

                        //if (fBaud > 3)
                        //{
                        //    ComboBox_baud.SelectedIndex = Convert.ToInt32(fBaud - 2);
                        //}
                        //else
                        //{
                        //    ComboBox_baud.SelectedIndex = Convert.ToInt32(fBaud);
                        //}

                        //Button3_Click(sender, e); //Automatically read the reader information (自动执行读取写卡器信息)

                        if ((fCmdRet == 0x35) | (fCmdRet == 0x30))
                        {
                            MessageBox.Show("Serial Communication Error or Occupied", "Information");
                            StaticClassReaderB.CloseSpecComPort(frmcomportindex);
                            ComOpen = false;
                        }
                    }
                }
                else
                {
                    temp = cmb_COM.SelectedItem.ToString();
                    temp = temp.Trim();
                    port = Convert.ToInt32(temp.Substring(3, temp.Length - 3));
                    for (i = 6; i >= 0; i--)
                    {
                        fBaud = Convert.ToByte(i);
                        if (fBaud == 3)
                            continue;
                        openresult = StaticClassReaderB.OpenComPort(port, ref fComAdr, fBaud, ref frmcomportindex);
                        fOpenComIndex = frmcomportindex;
                        if (openresult == 0x35)
                        {
                            MessageBox.Show("COM Opened", "Information");
                            return;
                        }
                        if (openresult == 0)
                        {
                            ComOpen = true;

                            //Button3_Click(sender, e); //Automatically read the reader information (自动执行读取写卡器信息)

                            //if (fBaud > 3)
                            //{
                            //    ComboBox_baud.SelectedIndex = Convert.ToInt32(fBaud - 2);
                            //}
                            //else
                            //{
                            //    ComboBox_baud.SelectedIndex = Convert.ToInt32(fBaud);
                            //}

                            if ((fCmdRet == 0x35) || (fCmdRet == 0x30))
                            {
                                ComOpen = false;
                                MessageBox.Show("Serial Communication Error or Occupied", "Information");
                                StaticClassReaderB.CloseSpecComPort(frmcomportindex);
                                return;
                            }
                            refreshStatus();
                            break;
                        }
                    }
                }
            }

            finally

            {
                Cursor = Cursors.Default;
            }

            if ((fOpenComIndex != -1) & (openresult != 0X35) & (openresult != 0X30))
            {
                cmb_OpenedCOM.Items.Add("COM" + Convert.ToString(fOpenComIndex));
                cmb_OpenedCOM.SelectedIndex = cmb_OpenedCOM.SelectedIndex + 1;

                btn_QueryTag.Enabled = true;
                ComOpen = true;

                //Button3.Enabled = true;
                //button20.Enabled = true;
                //Button5.Enabled = true;
                //Button1.Enabled = true;

                //Button_WriteEPC_G2.Enabled = true;
                //Button_SetMultiReadProtect_G2.Enabled = true;
                //Button_RemoveReadProtect_G2.Enabled = true;
                //Button_CheckReadProtected_G2.Enabled = true;
                //button4.Enabled = true;
                //SpeedButton_Query_6B.Enabled = true;
                //button6.Enabled = true;
                //button8.Enabled = true;
                //button9.Enabled = true;
                //button12.Enabled = true;
                //button_OffsetTime.Enabled = true;
                //button_settigtime.Enabled = true;
                //button_gettigtime.Enabled = true
            }
            if ((fOpenComIndex == -1) && (openresult == 0x30))
                MessageBox.Show("Serial Communication Error", "Information");

            if ((cmb_OpenedCOM.Items.Count != 0) & (fOpenComIndex != -1) & (openresult != 0X35) & (openresult != 0X30) & (fCmdRet == 0))
            {
                //fComAdr = Convert.ToByte(Edit_ComAdr.Text, 16);
                temp = cmb_OpenedCOM.SelectedItem.ToString();
                frmcomportindex = Convert.ToInt32(temp.Substring(3, temp.Length - 3));
            }
            refreshStatus();
        }

        private void btn_CloseComPort_Click(object sender, EventArgs e)
        {
            int port;
            //String Select COM Port ;
            string temp;
            clearLastInfo();
            try
            {
                if (cmb_OpenedCOM.SelectedIndex < 0)
                {
                    MessageBox.Show("Please Choose COM Port to close", "Information");
                }
                else
                {
                    temp = cmb_OpenedCOM.SelectedItem.ToString();
                    port = Convert.ToInt32(temp.Substring(3, temp.Length - 3));
                    fCmdRet = StaticClassReaderB.CloseSpecComPort(port);
                    if (fCmdRet == 0)
                    {
                        cmb_OpenedCOM.Items.RemoveAt(0);
                        if (cmb_OpenedCOM.Items.Count != 0)
                        {
                            temp = cmb_OpenedCOM.SelectedItem.ToString();
                            port = Convert.ToInt32(temp.Substring(3, temp.Length - 3));
                            StaticClassReaderB.CloseSpecComPort(port);
                            fComAdr = 0xFF;
                            StaticClassReaderB.OpenComPort(port, ref fComAdr, fBaud, ref frmcomportindex);
                            fOpenComIndex = frmcomportindex;
                            refreshStatus();
                            //Button3_Click(sender, e); //Automatically read the reader information (自动执行读取写卡器信息)
                        }
                    }
                    else
                        MessageBox.Show("Serial Communication Error", "Information");
                }
            }
            finally
            {

            }
            if (cmb_OpenedCOM.Items.Count != 0)
                cmb_OpenedCOM.SelectedIndex = 0;
            else
            {
                fOpenComIndex = -1;
                cmb_OpenedCOM.Items.Clear();
                cmb_OpenedCOM.Refresh();
                refreshStatus();

                //Button3.Enabled = false;
                //button20.Enabled = false;
                //Button5.Enabled = false;
                //Button1.Enabled = false;

                btn_QueryTag.Enabled = false;

                //Button_DestroyCard.Enabled = false;
                //Button_WriteEPC_G2.Enabled = false;
                //Button_SetReadProtect_G2.Enabled = false;
                //Button_SetMultiReadProtect_G2.Enabled = false;
                //Button_RemoveReadProtect_G2.Enabled = false;
                //Button_CheckReadProtected_G2.Enabled = false;
                //Button_SetEASAlarm_G2.Enabled = false;
                //button4.Enabled = false;

                //Button_LockUserBlock_G2.Enabled = false;
                //SpeedButton_Read_G2.Enabled = false;
                //Button_DataWrite.Enabled = false;
                //BlockWrite.Enabled = false;
                //Button_BlockErase.Enabled = false;
                //Button_SetProtectState.Enabled = false;
                //SpeedButton_Query_6B.Enabled = false;
                //SpeedButton_Read_6B.Enabled = false;
                //SpeedButton_Write_6B.Enabled = false;
                //Button14.Enabled = false;
                //Button15.Enabled = false;

                //DestroyCode.Enabled = false;
                //AccessCode.Enabled = false;
                //NoProect.Enabled = false;
                //Proect.Enabled = false;
                //Always.Enabled = false;
                //AlwaysNot.Enabled = false;
                //NoProect2.Enabled = false;
                //Proect2.Enabled = false;
                //Always2.Enabled = false;
                //AlwaysNot2.Enabled = false;

                //P_Reserve.Enabled = false;
                //P_EPC.Enabled = false;
                //P_TID.Enabled = false;
                //P_User.Enabled = false;
                //Alarm_G2.Enabled = false;
                //NoAlarm_G2.Enabled = false;

                //Same_6B.Enabled = false;
                //Different_6B.Enabled = false;
                //Less_6B.Enabled = false;
                //Greater_6B.Enabled = false;
                //button6.Enabled = false;
                //button8.Enabled = false;
                //button9.Enabled = false;

                ListView1_EPC.Items.Clear();

                //ComboBox_EPC1.Items.Clear();
                //ComboBox_EPC2.Items.Clear();
                //ComboBox_EPC3.Items.Clear();
                //ComboBox_EPC4.Items.Clear();
                //ComboBox_EPC5.Items.Clear();
                //ComboBox_EPC6.Items.Clear();

                btn_QueryTag.Text = "Stop2";
                chb_TID.Enabled = false;

                //SpeedButton_Read_6B.Enabled = false;
                //SpeedButton_Write_6B.Enabled = false;
                //Button14.Enabled = false;
                //Button15.Enabled = false;
                //ListView_ID_6B.Items.Clear();

                ComOpen = false;

                //button12.Enabled = false;
                //button10.Text = "Get";
                //button10.Enabled = false;
                //button11.Enabled = false;

                timer1.Enabled = false;

                //comboBox4.SelectedIndex = 0;
                //button_OffsetTime.Enabled = false;
                //button_settigtime.Enabled = false;
                //button_gettigtime.Enabled = false;
            }
        }

        public void ChangeSubItem(ListViewItem ListItem, int subItemIndex, string ItemText)
        {
            if (subItemIndex == 1)
            {
                if (ItemText == "")
                {
                    ListItem.SubItems[subItemIndex].Text = ItemText;
                    if (ListItem.SubItems[subItemIndex + 2].Text == "")
                    {
                        ListItem.SubItems[subItemIndex + 2].Text = "1";
                    }
                    else
                    {
                        ListItem.SubItems[subItemIndex + 2].Text = Convert.ToString(Convert.ToInt32(ListItem.SubItems[subItemIndex + 2].Text) + 1);
                    }
                }
                else
                if (ListItem.SubItems[subItemIndex].Text != ItemText)
                {
                    ListItem.SubItems[subItemIndex].Text = ItemText;
                    ListItem.SubItems[subItemIndex + 2].Text = "1";
                }
                else
                {
                    ListItem.SubItems[subItemIndex + 2].Text = Convert.ToString(Convert.ToInt32(ListItem.SubItems[subItemIndex + 2].Text) + 1);
                    if ((Convert.ToUInt32(ListItem.SubItems[subItemIndex + 2].Text) > 9999))
                        ListItem.SubItems[subItemIndex + 2].Text = "1";
                }

            }
            if (subItemIndex == 2)
            {
                if (ListItem.SubItems[subItemIndex].Text != ItemText)
                {
                    ListItem.SubItems[subItemIndex].Text = ItemText;
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer_test.Enabled = false;
            //Timer_G2_Read.Enabled = false;
            //Timer_G2_Alarm.Enabled = false;
            breakflag = true;
            fAppClosed = true;
            if (rdbtn_COM.Checked && frmcomportindex > 0)
            {
                StaticClassReaderB.CloseComPort();
            }
            if (rdbtn_TCP.Checked && frmcomportindex > 0)
            {
                StaticClassReaderB.CloseNetPort(frmcomportindex);
            }
        }

        private void cmb_QueryInterval_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_QueryInterval.SelectedIndex < 6)
                timer_test.Interval = 100;
            else
                timer_test.Interval = (cmb_QueryInterval.SelectedIndex + 4) * 10;
        }

        private void btn_QueryTag_Click(object sender, EventArgs e)
        {
            if (chb_TID.Checked)
            {
                if ((txt_StartAddress.Text.Length) != 2 || ((txt_LEN.Text.Length) != 2))
                {
                    toolStripStatusLabel1.Text = "TID Parameter Error！";
                    return;
                }
            }

            timer_test.Enabled = !timer_test.Enabled;

            if (!timer_test.Enabled)
            {
                txt_StartAddress.Enabled = true;
                txt_LEN.Enabled = true;
                chb_TID.Enabled = true;
                /*
                if (ListView1_EPC.Items.Count != 0)
                {
                    //DestroyCode.Enabled = false;
                    //AccessCode.Enabled = false;
                    //NoProect.Enabled = false;
                    //Proect.Enabled = false;
                    //Always.Enabled = false;
                    //AlwaysNot.Enabled = false;
                    //NoProect2.Enabled = true;
                    //Proect2.Enabled = true;
                    //Always2.Enabled = true;
                    //AlwaysNot2.Enabled = true;
                    //P_Reserve.Enabled = true;
                    //P_EPC.Enabled = true;
                    //P_TID.Enabled = true;
                    //P_User.Enabled = true;
                    //Button_DestroyCard.Enabled = true;
                    //Button_SetReadProtect_G2.Enabled = true;
                    //Button_SetEASAlarm_G2.Enabled = true;
                    //Alarm_G2.Enabled = true;
                    //NoAlarm_G2.Enabled = true;
                    //Button_LockUserBlock_G2.Enabled = true;
                    //Button_WriteEPC_G2.Enabled = true;
                    //Button_SetMultiReadProtect_G2.Enabled = true;
                    //Button_RemoveReadProtect_G2.Enabled = true;
                    //Button_CheckReadProtected_G2.Enabled = true;
                    //button4.Enabled = true;
                    //SpeedButton_Read_G2.Enabled = true;
                    //Button_SetProtectState.Enabled = true;
                    //Button_DataWrite.Enabled = true;
                    //BlockWrite.Enabled = true;
                    //Button_BlockErase.Enabled = true;
                    //checkBox1.Enabled = true;
                }
                if (ListView1_EPC.Items.Count == 0)
                {
                    //DestroyCode.Enabled = false;
                    //AccessCode.Enabled = false;
                    //NoProect.Enabled = false;
                    //Proect.Enabled = false;
                    //Always.Enabled = false;
                    //AlwaysNot.Enabled = false;
                    //NoProect2.Enabled = false;
                    //Proect2.Enabled = false;
                    //Always2.Enabled = false;
                    //AlwaysNot2.Enabled = false;
                    //P_Reserve.Enabled = false;
                    //P_EPC.Enabled = false;
                    //P_TID.Enabled = false;
                    //P_User.Enabled = false;
                    //Button_DestroyCard.Enabled = false;
                    //Button_SetReadProtect_G2.Enabled = false;
                    //Button_SetEASAlarm_G2.Enabled = false;
                    //Alarm_G2.Enabled = false;
                    //NoAlarm_G2.Enabled = false;
                    //Button_LockUserBlock_G2.Enabled = false;
                    //SpeedButton_Read_G2.Enabled = false;
                    //Button_DataWrite.Enabled = false;
                    //BlockWrite.Enabled = false;
                    //Button_BlockErase.Enabled = false;
                    //Button_WriteEPC_G2.Enabled = true;
                    //Button_SetMultiReadProtect_G2.Enabled = true;
                    //Button_RemoveReadProtect_G2.Enabled = true;
                    //Button_CheckReadProtected_G2.Enabled = true;
                    //button4.Enabled = true;
                    //Button_SetProtectState.Enabled = false;
                    //checkBox1.Enabled = false;

                }
                */
                AddCmdLog("Inventory", "Exit Query", 0);
                btn_QueryTag.Text = "Query Tag";
            }
            else
            {
                txt_StartAddress.Enabled = false;
                txt_LEN.Enabled = false;
                chb_TID.Enabled = false;

                //DestroyCode.Enabled = false;
                //AccessCode.Enabled = false;
                //NoProect.Enabled = false;
                //Proect.Enabled = false;
                //Always.Enabled = false;
                //AlwaysNot.Enabled = false;
                //NoProect2.Enabled = false;
                //Proect2.Enabled = false;
                //Always2.Enabled = false;
                //AlwaysNot2.Enabled = false;
                //P_Reserve.Enabled = false;
                //P_EPC.Enabled = false;
                //P_TID.Enabled = false;
                //P_User.Enabled = false;
                //Button_WriteEPC_G2.Enabled = false;
                //Button_SetMultiReadProtect_G2.Enabled = false;
                //Button_RemoveReadProtect_G2.Enabled = false;
                //Button_CheckReadProtected_G2.Enabled = false;
                //button4.Enabled = false;

                //Button_DestroyCard.Enabled = false;
                //Button_SetReadProtect_G2.Enabled = false;
                //Button_SetEASAlarm_G2.Enabled = false;
                //Alarm_G2.Enabled = false;
                //NoAlarm_G2.Enabled = false;
                //Button_LockUserBlock_G2.Enabled = false;
                //SpeedButton_Read_G2.Enabled = false;
                //Button_DataWrite.Enabled = false;
                //BlockWrite.Enabled = false;
                //Button_BlockErase.Enabled = false;
                //Button_SetProtectState.Enabled = false;
                ListView1_EPC.Items.Clear();
                btn_QueryTag.Text = "Stop3";
                //ComboBox_EPC1.Items.Clear();
                //ComboBox_EPC2.Items.Clear();
                //ComboBox_EPC3.Items.Clear();
                //ComboBox_EPC4.Items.Clear();
                //ComboBox_EPC5.Items.Clear();
                //ComboBox_EPC6.Items.Clear();

                //checkBox1.Enabled = false;
            }
        }

        private void GetData()
        {
            byte[] ScanModeData = new byte[40960];
            int ValidDatalength, i;
            string temp, temps;
            ValidDatalength = 0;
            fCmdRet = StaticClassReaderB.ReadActiveModeData(ScanModeData, ref ValidDatalength, frmcomportindex);
            if (fCmdRet == 0)
            {
                temp = "";
                temps = ByteArrayToHexString(ScanModeData);
                for (i = 0; i < ValidDatalength; i++)
                {
                    temp = temp + temps.Substring(i * 2, 2) + " ";
                }

                //if (ValidDatalength > 0)
                //    listBox3.Items.Add(temp);

                //listBox3.SelectedIndex = listBox3.Items.Count - 1;
                toolStripStatusLabel1.Text = DateTime.Now.ToLongTimeString() + " Operation Success (操作成功)";
                //StatusBar1.Panels[0].Text = DateTime.Now.ToLongTimeString() + " Operation Success (操作成功)";
            }
            else
                toolStripStatusLabel1.Text = DateTime.Now.ToLongTimeString() + " Operation Failed 操作失败";
            //StatusBar1.Panels[0].Text = DateTime.Now.ToLongTimeString() + " Operation Failed 操作失败
        }
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (fIsInventoryScan)
                fIsInventoryScan = true;

            GetData();

            if (fAppClosed)
                Close();

            fIsInventoryScan = false;
        }

        private void chb_TID_CheckedChanged(object sender, EventArgs e)
        {
            if (chb_TID.Checked)
            {
                grp_TIDParam.Enabled = true;
                txt_StartAddress.Enabled = true;
                txt_LEN.Enabled = true;
            }
            else
            {
                grp_TIDParam.Enabled = false;
                txt_StartAddress.Enabled = false;
                txt_LEN.Enabled = false;
            }

        }
    }
}
