﻿using System;

namespace Corenity_RFID
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdbtn_TCP = new System.Windows.Forms.RadioButton();
            this.rdbtn_COM = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_CloseComPort = new System.Windows.Forms.Button();
            this.cmb_OpenedCOM = new System.Windows.Forms.ComboBox();
            this.txt_ComAddress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmb_BaudInput = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_OpenComPort = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmb_COM = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txt_TCPAddress = new System.Windows.Forms.TextBox();
            this.txt_TCPIPAddress = new System.Windows.Forms.TextBox();
            this.txt_TCPPort = new System.Windows.Forms.TextBox();
            this.btn_CloseNetPort = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_OpenNetPort = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ListView1_EPC = new System.Windows.Forms.ListView();
            this.listViewCol_Number = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listViewCol_ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listViewCol_Length = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listViewCol_Times = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chb_TID = new System.Windows.Forms.CheckBox();
            this.grp_TIDParam = new System.Windows.Forms.GroupBox();
            this.txt_LEN = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_StartAddress = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_QueryTag = new System.Windows.Forms.Button();
            this.cmb_QueryInterval = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txt_AccPassword = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_WriteEPC = new System.Windows.Forms.TextBox();
            this.btn_WriteEPC = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer_test = new System.Windows.Forms.Timer(this.components);
            this.timer_G2Read = new System.Windows.Forms.Timer(this.components);
            this.timer_G2Alarm = new System.Windows.Forms.Timer(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btn_TestBuzzer = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btn_InsertDB = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.grp_TIDParam.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.rdbtn_TCP);
            this.groupBox1.Controls.Add(this.rdbtn_COM);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(146, 51);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "COMMUNICATION";
            // 
            // rdbtn_TCP
            // 
            this.rdbtn_TCP.AutoSize = true;
            this.rdbtn_TCP.Location = new System.Drawing.Point(64, 19);
            this.rdbtn_TCP.Name = "rdbtn_TCP";
            this.rdbtn_TCP.Size = new System.Drawing.Size(66, 22);
            this.rdbtn_TCP.TabIndex = 1;
            this.rdbtn_TCP.Text = "TCP/IP";
            this.rdbtn_TCP.UseVisualStyleBackColor = true;
            this.rdbtn_TCP.CheckedChanged += new System.EventHandler(this.rdbtn_TCP_CheckedChanged);
            // 
            // rdbtn_COM
            // 
            this.rdbtn_COM.AutoSize = true;
            this.rdbtn_COM.Location = new System.Drawing.Point(9, 19);
            this.rdbtn_COM.Name = "rdbtn_COM";
            this.rdbtn_COM.Size = new System.Drawing.Size(57, 22);
            this.rdbtn_COM.TabIndex = 0;
            this.rdbtn_COM.Text = "COM";
            this.rdbtn_COM.UseVisualStyleBackColor = true;
            this.rdbtn_COM.CheckedChanged += new System.EventHandler(this.rdbtn_COM_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.btn_CloseComPort);
            this.groupBox2.Controls.Add(this.cmb_OpenedCOM);
            this.groupBox2.Controls.Add(this.txt_ComAddress);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cmb_BaudInput);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.btn_OpenComPort);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cmb_COM);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 69);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(237, 236);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "COM";
            // 
            // btn_CloseComPort
            // 
            this.btn_CloseComPort.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btn_CloseComPort.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_CloseComPort.Location = new System.Drawing.Point(52, 195);
            this.btn_CloseComPort.Name = "btn_CloseComPort";
            this.btn_CloseComPort.Size = new System.Drawing.Size(133, 29);
            this.btn_CloseComPort.TabIndex = 9;
            this.btn_CloseComPort.Text = "Close Port";
            this.btn_CloseComPort.UseVisualStyleBackColor = false;
            this.btn_CloseComPort.Click += new System.EventHandler(this.btn_CloseComPort_Click);
            // 
            // cmb_OpenedCOM
            // 
            this.cmb_OpenedCOM.FormattingEnabled = true;
            this.cmb_OpenedCOM.Location = new System.Drawing.Point(139, 163);
            this.cmb_OpenedCOM.Name = "cmb_OpenedCOM";
            this.cmb_OpenedCOM.Size = new System.Drawing.Size(92, 26);
            this.cmb_OpenedCOM.TabIndex = 8;
            // 
            // txt_ComAddress
            // 
            this.txt_ComAddress.Location = new System.Drawing.Point(193, 49);
            this.txt_ComAddress.Name = "txt_ComAddress";
            this.txt_ComAddress.Size = new System.Drawing.Size(38, 26);
            this.txt_ComAddress.TabIndex = 10;
            this.txt_ComAddress.Text = "FF";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 18);
            this.label4.TabIndex = 7;
            this.label4.Text = "Opened COM Port :";
            // 
            // cmb_BaudInput
            // 
            this.cmb_BaudInput.FormattingEnabled = true;
            this.cmb_BaudInput.Items.AddRange(new object[] {
            "9600bps",
            "19200bps",
            "38400bps",
            "57600bps",
            "115200bps"});
            this.cmb_BaudInput.Location = new System.Drawing.Point(112, 120);
            this.cmb_BaudInput.Name = "cmb_BaudInput";
            this.cmb_BaudInput.Size = new System.Drawing.Size(119, 26);
            this.cmb_BaudInput.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Baud Rate :";
            // 
            // btn_OpenComPort
            // 
            this.btn_OpenComPort.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btn_OpenComPort.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_OpenComPort.Location = new System.Drawing.Point(52, 83);
            this.btn_OpenComPort.Name = "btn_OpenComPort";
            this.btn_OpenComPort.Size = new System.Drawing.Size(133, 29);
            this.btn_OpenComPort.TabIndex = 4;
            this.btn_OpenComPort.Text = "Open COM Port";
            this.btn_OpenComPort.UseVisualStyleBackColor = false;
            this.btn_OpenComPort.Click += new System.EventHandler(this.btn_OpenComPort_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Reader Address :";
            // 
            // cmb_COM
            // 
            this.cmb_COM.BackColor = System.Drawing.SystemColors.Window;
            this.cmb_COM.FormattingEnabled = true;
            this.cmb_COM.Location = new System.Drawing.Point(139, 19);
            this.cmb_COM.Name = "cmb_COM";
            this.cmb_COM.Size = new System.Drawing.Size(92, 26);
            this.cmb_COM.TabIndex = 1;
            this.cmb_COM.SelectedIndexChanged += new System.EventHandler(this.cmb_COM_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "COM port : ";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.txt_TCPAddress);
            this.groupBox3.Controls.Add(this.txt_TCPIPAddress);
            this.groupBox3.Controls.Add(this.txt_TCPPort);
            this.groupBox3.Controls.Add(this.btn_CloseNetPort);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.btn_OpenNetPort);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(12, 311);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(237, 212);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "TCP/IP";
            // 
            // txt_TCPAddress
            // 
            this.txt_TCPAddress.Location = new System.Drawing.Point(112, 86);
            this.txt_TCPAddress.Name = "txt_TCPAddress";
            this.txt_TCPAddress.Size = new System.Drawing.Size(119, 26);
            this.txt_TCPAddress.TabIndex = 12;
            this.txt_TCPAddress.Text = "FF";
            // 
            // txt_TCPIPAddress
            // 
            this.txt_TCPIPAddress.Location = new System.Drawing.Point(64, 51);
            this.txt_TCPIPAddress.Name = "txt_TCPIPAddress";
            this.txt_TCPIPAddress.Size = new System.Drawing.Size(167, 26);
            this.txt_TCPIPAddress.TabIndex = 11;
            this.txt_TCPIPAddress.Text = "192.168.1.190";
            // 
            // txt_TCPPort
            // 
            this.txt_TCPPort.Location = new System.Drawing.Point(170, 19);
            this.txt_TCPPort.Name = "txt_TCPPort";
            this.txt_TCPPort.Size = new System.Drawing.Size(61, 26);
            this.txt_TCPPort.TabIndex = 10;
            this.txt_TCPPort.Text = "6000";
            // 
            // btn_CloseNetPort
            // 
            this.btn_CloseNetPort.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btn_CloseNetPort.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_CloseNetPort.Location = new System.Drawing.Point(52, 175);
            this.btn_CloseNetPort.Name = "btn_CloseNetPort";
            this.btn_CloseNetPort.Size = new System.Drawing.Size(133, 29);
            this.btn_CloseNetPort.TabIndex = 9;
            this.btn_CloseNetPort.Text = "Close Net Port";
            this.btn_CloseNetPort.UseVisualStyleBackColor = false;
            this.btn_CloseNetPort.Click += new System.EventHandler(this.btn_CloseNetPort_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 18);
            this.label6.TabIndex = 5;
            this.label6.Text = "Address :";
            // 
            // btn_OpenNetPort
            // 
            this.btn_OpenNetPort.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btn_OpenNetPort.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_OpenNetPort.Location = new System.Drawing.Point(52, 140);
            this.btn_OpenNetPort.Name = "btn_OpenNetPort";
            this.btn_OpenNetPort.Size = new System.Drawing.Size(133, 29);
            this.btn_OpenNetPort.TabIndex = 4;
            this.btn_OpenNetPort.Text = "Open Net Port";
            this.btn_OpenNetPort.UseVisualStyleBackColor = false;
            this.btn_OpenNetPort.Click += new System.EventHandler(this.btn_OpenNetPort_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 18);
            this.label7.TabIndex = 2;
            this.label7.Text = "IP :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 18);
            this.label8.TabIndex = 0;
            this.label8.Text = "Port : ";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.ListView1_EPC);
            this.groupBox4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(255, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(453, 511);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "List of EPC Tags";
            // 
            // ListView1_EPC
            // 
            this.ListView1_EPC.AccessibleRole = System.Windows.Forms.AccessibleRole.IpAddress;
            this.ListView1_EPC.AutoArrange = false;
            this.ListView1_EPC.BackColor = System.Drawing.Color.SkyBlue;
            this.ListView1_EPC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListView1_EPC.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.listViewCol_Number,
            this.listViewCol_ID,
            this.listViewCol_Length,
            this.listViewCol_Times});
            this.ListView1_EPC.Dock = System.Windows.Forms.DockStyle.Top;
            this.ListView1_EPC.FullRowSelect = true;
            this.ListView1_EPC.GridLines = true;
            this.ListView1_EPC.HideSelection = false;
            this.ListView1_EPC.Location = new System.Drawing.Point(3, 22);
            this.ListView1_EPC.Name = "ListView1_EPC";
            this.ListView1_EPC.Size = new System.Drawing.Size(447, 481);
            this.ListView1_EPC.TabIndex = 2;
            this.ListView1_EPC.UseCompatibleStateImageBehavior = false;
            this.ListView1_EPC.View = System.Windows.Forms.View.Details;
            // 
            // listViewCol_Number
            // 
            this.listViewCol_Number.Text = "No.";
            this.listViewCol_Number.Width = 40;
            // 
            // listViewCol_ID
            // 
            this.listViewCol_ID.Text = "ID";
            this.listViewCol_ID.Width = 200;
            // 
            // listViewCol_Length
            // 
            this.listViewCol_Length.Text = "EPCLength";
            this.listViewCol_Length.Width = 100;
            // 
            // listViewCol_Times
            // 
            this.listViewCol_Times.Text = "Times";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.chb_TID);
            this.groupBox5.Controls.Add(this.grp_TIDParam);
            this.groupBox5.Controls.Add(this.btn_QueryTag);
            this.groupBox5.Controls.Add(this.cmb_QueryInterval);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(714, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(317, 181);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Query Tag";
            // 
            // chb_TID
            // 
            this.chb_TID.AutoSize = true;
            this.chb_TID.Location = new System.Drawing.Point(214, 95);
            this.chb_TID.Name = "chb_TID";
            this.chb_TID.Size = new System.Drawing.Size(47, 22);
            this.chb_TID.TabIndex = 14;
            this.chb_TID.Text = "TID";
            this.chb_TID.UseVisualStyleBackColor = true;
            this.chb_TID.CheckedChanged += new System.EventHandler(this.chb_TID_CheckedChanged);
            // 
            // grp_TIDParam
            // 
            this.grp_TIDParam.BackColor = System.Drawing.Color.Transparent;
            this.grp_TIDParam.Controls.Add(this.txt_LEN);
            this.grp_TIDParam.Controls.Add(this.label9);
            this.grp_TIDParam.Controls.Add(this.txt_StartAddress);
            this.grp_TIDParam.Controls.Add(this.label10);
            this.grp_TIDParam.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grp_TIDParam.Location = new System.Drawing.Point(9, 69);
            this.grp_TIDParam.Name = "grp_TIDParam";
            this.grp_TIDParam.Size = new System.Drawing.Size(199, 100);
            this.grp_TIDParam.TabIndex = 13;
            this.grp_TIDParam.TabStop = false;
            this.grp_TIDParam.Text = "Query TID Parameter";
            // 
            // txt_LEN
            // 
            this.txt_LEN.Enabled = false;
            this.txt_LEN.Location = new System.Drawing.Point(105, 54);
            this.txt_LEN.Name = "txt_LEN";
            this.txt_LEN.Size = new System.Drawing.Size(61, 26);
            this.txt_LEN.TabIndex = 14;
            this.txt_LEN.Text = "04";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 18);
            this.label9.TabIndex = 13;
            this.label9.Text = "Len : ";
            // 
            // txt_StartAddress
            // 
            this.txt_StartAddress.Enabled = false;
            this.txt_StartAddress.Location = new System.Drawing.Point(105, 24);
            this.txt_StartAddress.Name = "txt_StartAddress";
            this.txt_StartAddress.Size = new System.Drawing.Size(61, 26);
            this.txt_StartAddress.TabIndex = 12;
            this.txt_StartAddress.Text = "02";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 18);
            this.label10.TabIndex = 3;
            this.label10.Text = "Start Address :";
            // 
            // btn_QueryTag
            // 
            this.btn_QueryTag.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btn_QueryTag.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_QueryTag.Location = new System.Drawing.Point(230, 21);
            this.btn_QueryTag.Name = "btn_QueryTag";
            this.btn_QueryTag.Size = new System.Drawing.Size(77, 32);
            this.btn_QueryTag.TabIndex = 4;
            this.btn_QueryTag.Text = "Query Tag";
            this.btn_QueryTag.UseVisualStyleBackColor = false;
            this.btn_QueryTag.Click += new System.EventHandler(this.btn_QueryTag_Click);
            // 
            // cmb_QueryInterval
            // 
            this.cmb_QueryInterval.FormattingEnabled = true;
            this.cmb_QueryInterval.Location = new System.Drawing.Point(129, 25);
            this.cmb_QueryInterval.Name = "cmb_QueryInterval";
            this.cmb_QueryInterval.Size = new System.Drawing.Size(95, 26);
            this.cmb_QueryInterval.TabIndex = 1;
            this.cmb_QueryInterval.SelectedIndexChanged += new System.EventHandler(this.cmb_QueryInterval_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 33);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 18);
            this.label11.TabIndex = 0;
            this.label11.Text = "Read Interval : ";
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Transparent;
            this.groupBox7.Controls.Add(this.txt_AccPassword);
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Controls.Add(this.txt_WriteEPC);
            this.groupBox7.Controls.Add(this.btn_WriteEPC);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(1048, 12);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(310, 181);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Write EPC";
            // 
            // txt_AccPassword
            // 
            this.txt_AccPassword.Location = new System.Drawing.Point(10, 100);
            this.txt_AccPassword.Name = "txt_AccPassword";
            this.txt_AccPassword.Size = new System.Drawing.Size(294, 26);
            this.txt_AccPassword.TabIndex = 17;
            this.txt_AccPassword.Text = "00000000";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(168, 18);
            this.label5.TabIndex = 16;
            this.label5.Text = "Access Password (8 Hex) : ";
            // 
            // txt_WriteEPC
            // 
            this.txt_WriteEPC.Location = new System.Drawing.Point(9, 44);
            this.txt_WriteEPC.Name = "txt_WriteEPC";
            this.txt_WriteEPC.Size = new System.Drawing.Size(294, 26);
            this.txt_WriteEPC.TabIndex = 15;
            this.txt_WriteEPC.Text = "0000";
            // 
            // btn_WriteEPC
            // 
            this.btn_WriteEPC.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btn_WriteEPC.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_WriteEPC.Location = new System.Drawing.Point(118, 137);
            this.btn_WriteEPC.Name = "btn_WriteEPC";
            this.btn_WriteEPC.Size = new System.Drawing.Size(77, 32);
            this.btn_WriteEPC.TabIndex = 4;
            this.btn_WriteEPC.Text = "Write";
            this.btn_WriteEPC.UseVisualStyleBackColor = false;
            this.btn_WriteEPC.Click += new System.EventHandler(this.btn_WriteEPC_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(159, 18);
            this.label13.TabIndex = 0;
            this.label13.Text = "Write EPC (0 - 15 Word) :";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripProgressBar1,
            this.toolStripStatusLabel3});
            this.statusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.statusStrip1.Location = new System.Drawing.Point(0, 551);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1370, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel2.Text = "toolStripStatusLabel2";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(300, 16);
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel3.Text = "toolStripStatusLabel3";
            // 
            // timer_test
            // 
            this.timer_test.Tick += new System.EventHandler(this.timer_test_Tick);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btn_TestBuzzer
            // 
            this.btn_TestBuzzer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_TestBuzzer.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TestBuzzer.Location = new System.Drawing.Point(717, 484);
            this.btn_TestBuzzer.Name = "btn_TestBuzzer";
            this.btn_TestBuzzer.Size = new System.Drawing.Size(127, 31);
            this.btn_TestBuzzer.TabIndex = 8;
            this.btn_TestBuzzer.Text = "Buzzer Test";
            this.btn_TestBuzzer.UseVisualStyleBackColor = true;
            this.btn_TestBuzzer.Click += new System.EventHandler(this.btn_TestBuzzer_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.btn_InsertDB);
            this.groupBox6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(1045, 459);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(310, 64);
            this.groupBox6.TabIndex = 9;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Insert data to DB";
            this.groupBox6.Enter += new System.EventHandler(this.groupBox6_Enter);
            // 
            // btn_InsertDB
            // 
            this.btn_InsertDB.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btn_InsertDB.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_InsertDB.Location = new System.Drawing.Point(100, 25);
            this.btn_InsertDB.Name = "btn_InsertDB";
            this.btn_InsertDB.Size = new System.Drawing.Size(113, 32);
            this.btn_InsertDB.TabIndex = 4;
            this.btn_InsertDB.Text = "Insert";
            this.btn_InsertDB.UseVisualStyleBackColor = false;
            this.btn_InsertDB.Click += new System.EventHandler(this.btn_InsertDB_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Transparent;
            this.groupBox8.Controls.Add(this.listView1);
            this.groupBox8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(714, 199);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(644, 251);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "List of EPC Tags";
            // 
            // listView1
            // 
            this.listView1.AccessibleRole = System.Windows.Forms.AccessibleRole.IpAddress;
            this.listView1.AutoArrange = false;
            this.listView1.BackColor = System.Drawing.Color.SkyBlue;
            this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Top;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(3, 22);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(638, 223);
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "No.";
            this.columnHeader1.Width = 39;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Item ID";
            this.columnHeader2.Width = 202;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Product Name";
            this.columnHeader3.Width = 327;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Quantity";
            this.columnHeader4.Width = 67;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1370, 573);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.btn_TestBuzzer);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Corenity-RFID";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.grp_TIDParam.ResumeLayout(false);
            this.grp_TIDParam.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdbtn_TCP;
        private System.Windows.Forms.RadioButton rdbtn_COM;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmb_COM;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_OpenComPort;
        private System.Windows.Forms.Button btn_CloseComPort;
        private System.Windows.Forms.ComboBox cmb_OpenedCOM;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmb_BaudInput;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_CloseNetPort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_OpenNetPort;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_TCPAddress;
        private System.Windows.Forms.TextBox txt_TCPIPAddress;
        private System.Windows.Forms.TextBox txt_TCPPort;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListView ListView1_EPC;
        private System.Windows.Forms.ColumnHeader listViewCol_Number;
        private System.Windows.Forms.ColumnHeader listViewCol_ID;
        private System.Windows.Forms.ColumnHeader listViewCol_Length;
        private System.Windows.Forms.ColumnHeader listViewCol_Times;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btn_QueryTag;
        private System.Windows.Forms.ComboBox cmb_QueryInterval;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chb_TID;
        private System.Windows.Forms.GroupBox grp_TIDParam;
        private System.Windows.Forms.TextBox txt_LEN;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_StartAddress;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btn_WriteEPC;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txt_AccPassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_WriteEPC;
        private System.Windows.Forms.TextBox txt_ComAddress;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.Timer timer_test;
        private System.Windows.Forms.Timer timer_G2Read;
        private System.Windows.Forms.Timer timer_G2Alarm;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btn_TestBuzzer;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btn_InsertDB;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
    }
}

